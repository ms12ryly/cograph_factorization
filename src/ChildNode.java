import java.math.BigInteger;

public class ChildNode extends TreeNode{

    private int index;

    public ChildNode(int index) {
        super(false);
        this.index = index;
        this.setLeafCount(1);
    }

    public int getIndex(){
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    protected BigInteger calcNewick() {
        return BigInteger.TWO;
    }

    @Override
    public int calcLeafs() {
        return 1;
    }
}
