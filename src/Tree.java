import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Tree {

    private TreeNode tree;
    private int leafs;

    /**
     * @param root
     * @param init tells, whether ids, leafCount and children should be calculated
     */
    public Tree(TreeNode root, boolean init) {
        this.tree = root;

        if (init) {
            leafs = tree.calcLeafs();
            calcIds();
            tree.sortChildrenById();
        } else {
            leafs = 0;
            for (TreeNode c : tree.getChildren()) {
                leafs += c.getLeafCount();
            }
            tree.setLeafCount(leafs);
        }
    }


    public List<List<FactorTree>> factorizeGraph() {
        List<List<FactorTree>> ret = new LinkedList<>();

        List<Integer> factorSizes = new LinkedList<>();

        List<Tuple<Integer, Tree>> recursiveCalls = new ArrayList<>();

        kLoop:
        for (int k = leafs / 2; k >= 2; k--) {
            //k div n ?

            for(int kPrime : factorSizes){
                if(kPrime % k == 0){
                    continue kLoop;
                }
            }

            List<TreeNode> markedNodes = getMarkedNodes(k);

            //check labels
            boolean label = markedNodes.get(0).isLabel();
            for (TreeNode m : markedNodes) {
                if (m.isLabel() != label) {
                    continue kLoop;
                }
            }

            //check leafs
            for (TreeNode m : markedNodes) {
                if (m.getLeafCount() % k != 0) {
                    continue kLoop;
                }
            }


            List<TreeNode> secondFactorChildren = null;
            boolean secondFactorLabel = false;

            for (TreeNode m : markedNodes) {
                int finalK = k;
                List<TreeNode> children = m.getChildren().stream().filter(c -> c.getLeafCount() < finalK).toList();
                int childrenLeafs = children.stream().mapToInt(TreeNode::getLeafCount).sum();

                if (childrenLeafs % k != 0) {
                    System.out.println("error");
                    continue kLoop;
                }
                int numberOfCopies = childrenLeafs / k;
                if (children.size() % numberOfCopies != 0) {
                    System.out.println("error");
                    continue kLoop;
                }
                //TODO label check

                for (int i = 0; i < numberOfCopies; i++) {
                    if (secondFactorChildren == null) {
                        secondFactorChildren = new LinkedList<>();
                        secondFactorLabel = m.isLabel();

                        for (int j = 0; j < children.size() / numberOfCopies; j++) {
                            secondFactorChildren.add(children.get(j * numberOfCopies + i));
                        }
                    } else {
                        for (int j = 0; j < children.size() / numberOfCopies; j++) {
                            if (secondFactorChildren.get(j).getId() != children.get(j * numberOfCopies + i).getId()) {
                                continue kLoop;
                            }
                        }
                    }

                }

            }

            assert secondFactorChildren != null;

            TreeNode secondFactor = new TreeNode(secondFactorLabel);
            for(TreeNode tn : secondFactorChildren){
                secondFactor.addChild(tn);
            }

            Tree factorTree = new Tree(secondFactor, false);

            recursiveCalls.add(new Tuple<>(k, factorTree));

            factorSizes.add(k);


        }

        //omp parallel for
        for(int i = 0; i<recursiveCalls.size(); i++){
            int k = recursiveCalls.get(i).getA();
            Tree factorTree = recursiveCalls.get(i).getB();

            List<List<FactorTree>> subFactors = factorTree.factorizeGraph();
            for (List<FactorTree> subFactor : subFactors) {
                subFactor.add(0, new FactorTree(tree, k));
            }

            //omp critical
            {
                ret.addAll(subFactors);
            }
        }


        if(ret.size() == 0){
            List<FactorTree> factor = new LinkedList<>();
            factor.add(new FactorTree(this.tree, 0));
            ret.add(factor);
        }

        return ret;

    }

    //getRP
    private List<TreeNode> getMarkedNodes(int k) {
        List<TreeNode> markedNodes = new LinkedList<>();

        Stack<TreeNode> toCompute = new Stack<>();
        toCompute.push(tree);

        while (!toCompute.isEmpty()) {
            TreeNode t = toCompute.pop();
            if (t.getLeafCount() < k) {
                continue;
            }
            boolean hasChildWithLessLeafs = false;
            for (TreeNode c : t.getChildren()) {
                if (c.getLeafCount() < k) {
                    hasChildWithLessLeafs = true;
                    break;
                }
            }

            if (hasChildWithLessLeafs) {
                markedNodes.add(t);
            }
            for (TreeNode c : t.getChildren())
                toCompute.push(c);

        }

        return markedNodes;
    }

    private void calcIds() {
        Map<Integer, List<TreeNode>> depthMap = new HashMap<>();
        int maxDepth = tree.calcDepthAndAddToList(depthMap);

        int currentId = 1;
        //start with d = 1, because d = 0 has id 1

        // omp parallel for
        for(int d = 1; d<= maxDepth; d++){
            List<TreeNode> list = depthMap.get(d);
            list.sort((t0, t1) -> {
                int[] t0id = t0.getTempId();
                int[] t1id = t1.getTempId();
                if (t0id.length > t1id.length) {
                    return 1;
                } else if (t0id.length < t1id.length) {
                    return -1;
                } else {
                    for (int i = 0; i < t0id.length; i++) {
                        if (t0id[i] < t1id[i]) return -1;
                        else if (t0id[i] > t1id[i]) return 1;
                    }
                }
                return 0;
            });
        }

        for (int d = 1; d <= maxDepth; d++) {
            List<TreeNode> list = depthMap.get(d);
            currentId++;
            list.get(0).setId(currentId);
            for (int i = 1; i < list.size(); i++) {
                if (!Arrays.equals(list.get(i).getTempId(), list.get(i - 1).getTempId())) {
                    currentId++;
                }
                list.get(i).setId(currentId);
            }
        }
    }

    @Override
    public String toString() {
        return tree.toString();
    }
}
