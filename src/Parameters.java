
public class Parameters {

	String inputFile;
	String outputFile;
	
	public Parameters() {
		this.inputFile = null;
		this.outputFile = null;
	}
	
	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}
}
