import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class TreeBuilder {

    public TreeBuilder() {

    }

    private List<boolean[]> findeZusammenhangskomponenten(InputGraph graph, boolean[] activeNodes, boolean flipGraph){
        List<boolean[]> zusammenhangsKomponenten = new LinkedList<>();
        //Speichere bereits abgearbeitete Knoten
        boolean[] marked = new boolean[activeNodes.length];

        for(int i = 0; i<activeNodes.length; i++){

            if(!activeNodes[i] || marked[i]){
                continue; //Wenn Knoten bereits abgearbeitet ist
            }


            Stack<Integer> stack = new Stack<>();
            stack.push(i);
            marked[i] = true;
            boolean[] zshmKomponente = new boolean[activeNodes.length];

            while(!stack.isEmpty()){
                Integer cur = stack.pop();
                zshmKomponente[cur] = true;

                //marked[cur] = true;

                for(int j = 0; j<graph.adjacencies[cur].length; j++){
                    if(activeNodes[j] && graph.adjacencies[cur][j]^flipGraph && !marked[j]){
                        stack.push(j);
                        marked[j] = true;
                    }
                }

            }

            zusammenhangsKomponenten.add(zshmKomponente);

        }

        return zusammenhangsKomponenten;
    }

    //Too much overhead, so don't use this method
    private List<boolean[]> findeZusammenhangskomponenten_par(InputGraph graph, boolean[] activeNodes, boolean flipGraph) {

        List<boolean[]> zusammenhangsKomponenten = new LinkedList<>();
        //Speichere bereits abgearbeitete Knoten

        //omp parallel for
        for (int i = 0; i < activeNodes.length; i++) {
            if (activeNodes[i]) {

                boolean[] marked = new boolean[activeNodes.length];
                Stack<Integer> stack = new Stack<>();

                stack.push(i);
                marked[i] = true;

                boolean[] zshmKomponente = new boolean[activeNodes.length];
                boolean isZshmKomponente = true;

                while (!stack.isEmpty()) {
                    Integer cur = stack.pop();
                    zshmKomponente[cur] = true;

                    if (cur < i) {
                        isZshmKomponente = false;
                        break;
                    }
                    for (int j = 0; j < graph.adjacencies[cur].length; j++) {
                        if (activeNodes[j] && graph.adjacencies[cur][j] ^ flipGraph && !marked[j]) {
                            stack.push(j);
                            marked[j] = true;
                        }
                    }

                }

                //omp critical
                {
                    if (isZshmKomponente) {
                        zusammenhangsKomponenten.add(zshmKomponente);
                    }
                }

            }
        }

        return zusammenhangsKomponenten;
    }


    private int getIndexIfChild(boolean[] nodes) {
        int c = 0;
        int pos = -1;
        for (int i = 0; i < nodes.length && c < 2; i++) {
            if (nodes[i]) {
                c++;
                pos = i;
            }
        }

        return c < 2 ? pos : -1;
    }

    private TreeNode buildSubTree(InputGraph graph, boolean[] activeNodes) {
        int childIndex = getIndexIfChild(activeNodes);
        if (childIndex != -1) {
            return new ChildNode(childIndex);
        }

        boolean label = false;
        List<boolean[]> zshk = findeZusammenhangskomponenten(graph, activeNodes, false);

        if (zshk.size() == 1) {
            zshk = findeZusammenhangskomponenten(graph, activeNodes, true);
            label = true;
        }

        TreeNode root = new TreeNode(label);
        TreeNode[] children = new TreeNode[zshk.size()];

        //omp parallel for
        for (int i = 0; i < zshk.size(); i++) {
            children[i] = buildSubTree(graph, zshk.get(i));
        }

        for (TreeNode c : children) {
            root.addChild(c);
        }

        return root;
    }

    public TreeNode buildTree(InputGraph graph) {

        boolean[] allNodes = new boolean[graph.noVertices];
        //omp parallel for
        for (int i = 0; i < allNodes.length; i++) {
            allNodes[i] = true;
        }

        return buildSubTree(graph, allNodes);

    }
}
