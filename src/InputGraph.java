import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class InputGraph {

	int noVertices;
	boolean[][] adjacencies;
	
	public InputGraph(Parameters parameters) throws IOException{
		String filename = parameters.getInputFile();
		this.noVertices = 0;
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String header = br.readLine();

		if(header == null || header.charAt(0) != '#'){
			return;
		}

		this.noVertices = Integer.parseInt(header.substring(1).trim());
		this.adjacencies = new boolean[this.noVertices][this.noVertices];

		String[] lines = new String[noVertices];

		//there is no omp ordered in omp4j so this part has to be sequential
		for(int i = 0; i<noVertices; i++) {
			lines[i] = br.readLine();
		}

		//omp parallel for
		for(int i = 0; i<noVertices; i++){
			String line = lines[i];
			if(!line.equals("") && !line.startsWith("#")){
				int j = 0;
				for(char c : line.toCharArray()){
					if(c == '0' || c == '1'){
						this.adjacencies[i][j]= !(c == '0');
						j++;
					}
				}
			}
		}
		br.close();
	}

}
