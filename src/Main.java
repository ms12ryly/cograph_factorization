import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static String version = "0.1";

    public static void main(String[] args) throws IOException {


        // omp parallel for
        for (int i=0; i<10;i++)
        {
            System.out.println(i);
        }

        long time = System.currentTimeMillis();

        // create the command line parser
        Parameters parameters = new Parameters();
        parameters.setInputFile(args[0]);

        InputGraph graph = new InputGraph(parameters);
        System.out.println("Tree read time:" + (System.currentTimeMillis() - time) + "ms");
        time = System.currentTimeMillis();
        TreeBuilder treeBuilder = new TreeBuilder();
        TreeNode treeNode = treeBuilder.buildTree(graph);
        System.out.println("Tree build time:" + (System.currentTimeMillis() - time) + "ms");
        time = System.currentTimeMillis();
        Tree tree = new Tree(treeNode, true);
        List<List<FactorTree>> decomposition = tree.factorizeGraph();

        for(List<FactorTree> d : decomposition){
            System.out.println("Decomposition found: ");
            for(FactorTree f: d){
                System.out.println(f.toString());
            }
        }

        //System.out.println(tree);

        PrintStream printStream = System.out;
        if (parameters.getOutputFile() != null) {
            String output = parameters.getOutputFile();
            printStream = new PrintStream(new FileOutputStream(output, false));
        }

        //TODO: do factorization

        printStream.flush();
        System.out.println("\n#Computation time: " + (System.currentTimeMillis() - time) + " millisec");
        printStream.close();
    }

}
