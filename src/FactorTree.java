public class FactorTree {

    private TreeNode base;
    private int minLeafCount;

    public FactorTree(TreeNode base, int minLeafCount){
        this.base = base;
        this.minLeafCount = minLeafCount;
    }

    @Override
    public String toString(){
        return base.toStringUntilMarked(minLeafCount);
    }

}
