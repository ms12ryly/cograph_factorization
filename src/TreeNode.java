import java.math.BigInteger;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TreeNode {
    private boolean label;
    private List<TreeNode> children;
    private int depth = -1;
    private int id = 1;
    private int[] tempId = null;
    private int leafCount = -1;

    private BigInteger newick = null;

    public TreeNode(boolean label) {
        children = new LinkedList<>();
        this.label = label;
    }

    public void addChild(TreeNode child) {
        children.add(child);
    }

    public boolean isLabel() {
        return label;
    }

    public void setLabel(boolean label) {
        this.label = label;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLeafCount() {
        return leafCount;
    }

    public void setLeafCount(int leafCount) {
        this.leafCount = leafCount;
    }


    public void sortChildrenById() {
        for (TreeNode c : children) {
            c.sortChildrenById();
        }
        children.sort((c1, c2) -> c2.id - c1.id);
    }

    public int[] getTempId() {
        if (tempId == null) {
            tempId = new int[children.size()];
            for (int i = 0; i < children.size(); i++) {
                tempId[i] = children.get(i).getId();
            }
            Arrays.sort(tempId);
        }
        return tempId;
    }

    protected BigInteger calcNewick() {
        //todo
        children.sort(Comparator.comparing(TreeNode::getNewick));

        int newExponent = children.stream().mapToInt((a) -> a.getNewick().bitLength()).sum() + 1;
        int currentExponent = 1;

        BigInteger result = BigInteger.TWO.pow(newExponent);

        for (TreeNode child : children) {
            result = result.add(child.getNewick().multiply(BigInteger.TWO.pow(currentExponent)));
            currentExponent += child.getNewick().bitLength();
        }

        return result;
    }

    public BigInteger getNewick() {
        if (newick == null) {
            this.newick = calcNewick();
        }
        return newick;
    }

    public int calcDepthAndAddToList(Map<Integer, List<TreeNode>> map) {
        depth = 0;
        for (TreeNode c : children) {
            int cDepth = c.calcDepthAndAddToList(map);
            if (cDepth + 1 > depth) {
                depth = cDepth + 1;
            }
        }

        if (!map.containsKey(depth)) {
            map.put(depth, new LinkedList<>());
        }
        map.get(depth).add(this);

        return depth;
    }

    public int calcLeafs() {
        leafCount = 0;
        for (TreeNode child : children) {
            leafCount += child.calcLeafs();
        }
        return leafCount;
    }

    @Override
    public String toString() {
        return toStringUntilMarked(0);
    }

    public String toStringUntilMarked(int minLeafCount) {
        StringBuilder buffer = new StringBuilder(50);
        printUntilMarked(buffer, "", "", minLeafCount);
        return buffer.toString();
    }

    private void printUntilMarked(StringBuilder buffer, String prefix, String childrenPrefix, int minLeafCount) {
        buffer.append(prefix);
        buffer.append(label);
        buffer.append(" id:").append(id);
        buffer.append(" depth:").append(depth);
        buffer.append('\n');
        int additionalLeafs = 0;
        for (Iterator<TreeNode> it = children.iterator(); it.hasNext(); ) {
            TreeNode next = it.next();
            if (next.leafCount > minLeafCount) {
                if (it.hasNext()) {
                    next.printUntilMarked(buffer, childrenPrefix + "├── ", childrenPrefix + "│   ", minLeafCount);
                } else {
                    next.printUntilMarked(buffer, childrenPrefix + "└── ", childrenPrefix + "    ", minLeafCount);
                }
            }else{
                additionalLeafs += next.leafCount;
            }
        }

        for(int i = 0; minLeafCount != 0 && i<additionalLeafs/minLeafCount; i++){
            buffer.append(childrenPrefix).append("├── ");
            buffer.append("*");
            buffer.append("\n");
        }
    }
}
