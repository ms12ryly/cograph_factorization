input_files = ["graph", "graph2", "graph3", "graph"]

input_graphs = []
size = 1

for i in input_files:
    f = open(i)
    f.readline()
    lines = f.readlines()
    size = size * len(lines)
    for k in range(0, len(lines)):
        l = lines[k]
        l = l.replace(" ", "")
        l = l.replace("\n", "")
        lines[k] = l
    input_graphs.append(lines)

outgraph = ["0"]

for graph in input_graphs:
    new_outgraph = []
    for line in range(0, len(outgraph)):
        for y in range(0, len(graph)):
            new_outgraph.append("")
            for zeichen in range(0, len(outgraph[line])):
                for x in range(0, len(graph[y])):
                    if line == zeichen:
                        new_outgraph[line * len(graph) + y] += graph[x][y]
                    else:
                        new_outgraph[line *len(graph) + y] += outgraph[line][zeichen]

    outgraph = new_outgraph

print("#" + str(size))
for l in outgraph:
    print(l.replace("1", "1 ").replace("0", "0 "))


