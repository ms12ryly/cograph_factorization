#!/bin/bash
export GRAPH_FILE=graph3
export REPEATS=10
echo Time parallel:
time for i in $(seq 0 $REPEATS); do java -cp classes Main $GRAPH_FILE > /dev/null; done
echo $CMD_DURATION
echo Time sequential:
time for i in $(seq 0 $REPEATS); do java -cp bin Main $GRAPH_FILE > /dev/null; done
echo $CMD_DURATION
